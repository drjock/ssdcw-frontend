import React from 'react';
import logo from './logo.svg';
import './App.css';
import { AuthProvider } from './authContext';
import LoginPage from './components/login';
import homepage from './components/home';
import PrivateRoute from './components/privateRoute';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <PrivateRoute path="/protected" component={homepage} />
          <Route path="/*" component={LoginPage} />
        </Switch>
      </Router>
    </AuthProvider>
  );
}

export default App;
