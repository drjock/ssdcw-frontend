import React, { useContext, useState } from 'react'

const authContext = React.createContext(null)

function useAuth() {
    return useContext(authContext);
}

const AuthProvider: React.FC = ({ children }) => {
    const auth = useAuthProvider();
    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
}

function useAuthProvider() {
    const [user, setUser] = useState(null);

    // with async example
    const signin = async () => {
        await new Promise(resolve => {
            setTimeout(() => resolve(), 1000)
        })
        setUser("Some User")
    };

    const signout = () => {
        setUser(null)
    };

    return {
        user,
        signin,
        signout
    };
}

export { AuthProvider, useAuth }