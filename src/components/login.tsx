import React, { useEffect } from 'react';
import { useAuth } from "../authContext";
import { useHistory } from 'react-router-dom';

export default function LoginPage() {

    let auth = useAuth()
    let history = useHistory()

    function login() {
        auth.signin()
    }

    useEffect(() => {
        if (auth.user) {
            history.push("/protected");
        }
    }, [auth.user])


    return (
        <div>
            <h1>Login</h1>
            {/* <form name='f' action="/process_login" method='POST'>
                <table>
                    <tr>
                        <td>User:</td>
                        <td><input type='text' name='username' value='' /></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type='password' name='password' /></td>
                    </tr>
                    <tr>
                        <td><input name="submit" type="submit" value="submit" /></td>
                    </tr>
                </table>
            </form> */}
            <button onClick={login}></button>
        </div>
    )
} 